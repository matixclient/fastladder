package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.player.PlayerMoveEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;

/**
 * Created by Lars on 26.07.2016.
 */
public class ModuleFastLadder extends Module {
  public ModuleFastLadder() {
    super("FastLadder", ModuleCategory.MOVEMENT);

    this.setVersion("1.0");
    this.setBuildVersion(16001);
    this.setDescription("Allows you to ascend ladders faster");
    this.setSyntax("fastladder");
  }

  @Override
  public void onEnable() {
    Wrapper.getEventManager().register(this);
  }

  @EventHandler
  public void onPlayerMove(PlayerMoveEvent moveEvent) {
    if (moveEvent.getPlayer().isOnLadder() && moveEvent.getPlayer().isCollidedHorizontally) {
      moveEvent.setMotionY(moveEvent.getMotionY() * 3F);
    }
  }

  @Override
  public void onDisable() {
    Wrapper.getEventManager().unregister(this);
  }
}
